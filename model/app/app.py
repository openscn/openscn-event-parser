'''
Describes an actual app.
'''

class App(object):
    def __init__(self, id, service, interface):
        self.id = id
        self.service = service
        self.interface = interface
