"""
Request is a message comming from the ouside world.
"""
# NOTE: For now is a request from the project API to save a value.

import json
from datetime import datetime
from flask_restful import Resource
from flask_jsonpify import jsonify

"""
Request class
params:
    bundle (dict): the bundled data that is parsed from the JSON request.

    job_id (int): The id of the job.
    crated_time (timestamp): The time that the Request object is created.
    lifecycle (array): The steps that the job has to take until is created.
    project_id (int): The id project that the value belongs to.
    sending_device (ing): The id of the device that sent the value.
    group (int): The id of the group that sent the value.
    value (Value obj): the actual value.
    received_time: the time that the value came at the system.
    # TODO: investigate if we have to store the time that the device sent the value.
    #       or if we should have a flag for this.
"""

class Request():
    def __init__(self, project_id, device, group, sensor, value, received_time):
        self.created_at = datetime.now()
        self.initialize( project_id, device, group, sensor, value, received_time)
        self.parse_lifecycle()

    def __str__(self):
        line = ''
        line += str(self.project_id) + '\n'
        line += str(self.device) + '\n'
        line += str(self.group) + '\n'
        line += str(self.sensor) + '\n'
        line += str(self.value) + '\n'
        line += str(self.received_time) + '\n'
        line += str([ item for item in self.lifecycle if self.lifecycle[item] is not None])

        return line

    '''
    Returns a list with the correct order of the lifecycle
    '''
    # TODO: write a more pyhtonic implementation.
    def getLifecycle(self):
        # return [item for item in self.lifecycle if self.lifecycle[item] is not None]
        orderLifecycle = []

        if 'create' in self.lifecycle:
            orderLifecycle.append(self.lifecycle['create'])
        if 'process' in self.lifecycle:
            orderLifecycle.append(self.lifecycle['process'])
        if 'store' in self.lifecycle:
            orderLifecycle.append(self.lifecycle['store'])
        if 'publish' in self.lifecycle:
            orderLifecycle.append(self.lifecycle['publish'])

        return orderLifecycle


    # REVIEW: those two functions
    def initialize(self, project_id, device, group, sensor, value, received_time):
        self.project_id = project_id
        self.device = device,
        self.group = group
        self.sensor = sensor
        self.value = value
        self.received_time = received_time

    """
    (Will) query the database to find out the lifecycle of the job.
    If not found, a 'standard' lifecycle is assumed.
    For now it supports just the 'standard' lifecycle.
    """
    def parse_lifecycle(self):
        self.lifecycle =  {
            'create': 'default',
            'process': None,
            'store': 'default',
            'publish': None
        }



# A request from the outsideworld should look like
# {
#     # NOTE: meta data
#     job_id: xxx-xxx-xxx-xxx,
#     created_time: xxxxxxx,
#     lifecycle: ['xxx', 'xxx', 'xxx', 'xxx'],
#     hooks: {
#         outgoing: xxxxx
#         incoming
#     },
#
#     # NOTE: data
#     Project_id: xxx-xxx-xxx-xxx,
#     sending_device: xxx-xxx-xxx-xxx,
#     group: xxx-xxx-xxx-xxx,
#     sensor: xxx-xxx-xxx-xxx,
#     value: xxxx,
#     received_time: xxxxxxxxx
# }
